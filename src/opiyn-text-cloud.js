(function (window, angular, undefined) { 'use strict';

  // Only create the module if it doesn't exist already
  try {
    angular.module('opiyn-text-cloud');
  }
  catch (error) {
    angular.module('opiyn-text-cloud', []);
  }

  angular
    .module('opiyn-text-cloud')
    .directive('oTextCloud', textCloudDirective);

  function textCloudDirective () {
    var directive = {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
      restrict: 'E',
      template:
        '<div class="o-text-cloud">'+
          '<span class="o-text-cloud-element" '+
            'ng-repeat="item in vm.content" '+
            'ng-class="vm.randomClass()" '+
            'ng-click="vm.click(item)" '+
            'ng-style="vm.style(item)">{{ item.text }}</span>'+
        '</div>',
      scope: {
        content: '=',
        onClick: '&'
      },
      controller: ['$element', '$log', TextCloudDirectiveController],
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    function TextCloudDirectiveController ($element, $log) {
      var vm = this;

      vm.click = function (item) {
        vm.onClick({ item: item });
      };

      vm.randomClass = function (){
       var rand = Math.floor((Math.random()*10)+1);       
        return "opiyn-font-" + rand;
      };

      vm.style = function (item) {
        var usage = item.usage;
        usage = 100 + usage * 20;
        return {
          'font-size': usage + '%'
        };
      };
    }
  }
})(window, window.angular);
