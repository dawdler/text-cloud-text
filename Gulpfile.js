var connect = require('connect');
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var livereload = require('gulp-livereload');
var minify = require('gulp-minifier');
var rename = require('gulp-rename');
var serveStatic = require('serve-static');

// Edit this values to best suit your app
var WEB_PORT = 9000;

// JS hint files
gulp.task('jshint', function() {
  gulp.src(['src/**/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter());
});

// Start local http server for development
gulp.task('http-server', function() {
  var app = connect();

  app.use(require('connect-livereload')());

  // Serve examples from root
  app.use(serveStatic('examples'));

  // Serve JavaScript files in place
  app.use(serveStatic('.'));

  app.listen(WEB_PORT);

  console.log('(o) - Server listening on http://localhost:' + WEB_PORT);
});

// Start watch for file changes
gulp.task('watch', function () {
  livereload.listen();
  var watchFiles = ['examples/**/*.html', 'src/**/*.css', 'src/**/*.js'];
  gulp.watch(watchFiles, function (e) {
    console.log('(o) - Files changed. Reloading...');
    gulp
      .src(watchFiles)
      .pipe(livereload());
  });
});

// Minify files for distribution
gulp.task('minify', function() {
  gulp
    .src('src/**/*')
    .pipe(minify({
      minify: true,
      collapseWhitespace: true,
      conservativeCollapse: true,
      minifyJS: true,
      minifyCSS: true,
      getKeptComment: function (content, filePath) {
        var m = content.match(/\/\*![\s\S]*?\*\//img);
        return m && m.join('\n') + '\n' || '';
      }
    }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist'));
});

// Start local http server with watch and livereload set up
gulp.task('server', ['watch', 'http-server']);

// For development purpose only
gulp.task('develop', ['jshint', 'server'], function () {
  console.log('(o) - Server started successfully');
});

// Default task is to build the Angular module
gulp.task('default', ['jshint', 'minify'], function () {
  console.log('(o) - Release build completed');
});
